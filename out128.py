#!/usr/bin/python3
# -*- coding: utf-8 -*-

# pcf8591 library with examples

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/pcf8591

import pcf8591

pcf8591.setOutValue(128)
