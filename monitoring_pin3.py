#! /usr/bin/python3
# -*- coding: utf-8 -*-

# pcf8591 library with examples

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/pcf8591

import pcf8591
from time import sleep
from os import system
from sys import exit

try:
  while True:
    system('clear')
    print("Stop with CTRL + C\n")
    pcf8591.dumpPin(3)
    sleep(1)
except KeyboardInterrupt:
  exit(0)
