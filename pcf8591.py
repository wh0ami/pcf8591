#! /usr/bin/python3
# -*- coding: utf-8 -*-

# pcf8591 library with examples

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/pcf8591


# only for usage with the PCF8591 A/D converter via I²C
# tested with RaspberryPi Zero WH

from smbus2 import SMBus

# used I²C bus
i2c = 1

# I²C address of the PCF8591
address = 0x48

# I²C bus object
bus = SMBus(i2c)


# function for checking whether the pin is valid
#
# Parameter:
# PCFpin = AIN pin (0-3)
def checkPin(PCFpin):
  pin = int(PCFpin)
  if pin<0 or pin>3:
    raise ValueError('Invalid pin number! Must be between 0 and 3!')
    return False
  return True

# function for getting the value of a specific pin
#
# Parameter:
# PCFpin = AIN pin (0-3)
def getValue(PCFpin):
  checkPin(PCFpin)

  global address
  global bus

  ain = 0x40 + int(PCFpin)

  bus.write_byte(address,ain)
  bus.read_byte(address)
  value = int(bus.read_byte(address))

  return value

# function for getting the voltage of a specific pin
#
# Parameter:
# PCFpin = AIN pin (0-3)
# precision = amount of returned decimal places
def getVoltage(PCFpin,precision=2):
  checkPin(PCFpin)

  global address
  global bus

  ain = 0x40 + int(PCFpin)

  bus.write_byte(address,ain)
  bus.read_byte(address)
  voltage = bus.read_byte(address)*(3.3/255)
  voltage = float(round(voltage,precision))

  return voltage

# function for printing the value and the voltage of a specific pin
#
# Parameter:
# PCFpin = AIN pin (0-3)
def dumpPin(PCFpin=0):
  checkPin(PCFpin)

  value = getValue(PCFpin)
  voltage = getVoltage(PCFpin)

  print("+++ Dump of PCF8591 pin "+str(PCFpin)+" +++")
  print("Value: "+str(value))
  print("Voltage: "+str(voltage)+"V")

  return True

# function for setting the value of AOUT
#
# Parameter:
# value = integer between 0 and 255
def setOutValue(value):
  if value<0 or value>255:
    raise ValueError('Invalid value! Must be between 0 and 255!')
    return False

  global address
  global bus

  bus.write_byte_data(address,0x40,value)

  return True

# function for setting the voltage of AOUT
#
# Parameter:
# value = float between 0.0 and 3.3
def setOutVoltage(value):
  if value<0.0 or value>3.3:
    raise ValueError('Invalid value! Must be between 0.0 and 3.3!')
    return False

  global address
  global bus

  value = int(round(value/(3.3/255),0))

  bus.write_byte_data(address,0x40,value)

  return True
