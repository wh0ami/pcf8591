# pcf8591

Python3 library for reading and writing from the PCF8591 A/D converter via I²C



The following content is specific for the YL-40 board.

| Pin  | Address | OnBoard device  | Jumper |
| ---- | ------- | --------------- | -------|
| AIN0 | 0x40    | light resistor  | P5     |
| AIN1 | 0x41    | thermistor      | P4     |
| AIN2 | 0x42    | normally closed | -      |
| AIN3 | 0x43    | potentiometer   | P6     |
| AOUT | 0x40    | -               | -      |

If the a jumper is connected, you will see the value of the onboard device. If you remove the jumper, you will see the value of the to the pin connected device.